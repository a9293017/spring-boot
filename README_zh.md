> 注：本系列源码分析基于springboot 2.2.2.RELEASE，对应的spring版本为5.2.2.RELEASE，源码的gitee仓库仓库地址：[funcy/spring-boot](https://gitee.com/funcy/spring-boot).
>
> 附：[spring 源码分析系列文章汇总](https://my.oschina.net/funcy/blog/4527454)
>

- [【springboot源码分析】搭建 springboot 源码分析环境](https://my.oschina.net/funcy/blog/4816981)
- [【springboot源码分析】@SpringBootApplication 注解](https://my.oschina.net/funcy/blog/4870882)

------------

- [【springboot源码分析】springboot 启动流程（一）：准备 SpringApplication](https://my.oschina.net/funcy/blog/4877610)
- [【springboot源码分析】springboot 启动流程（二）：准备运行环境](https://my.oschina.net/funcy/blog/4882417)
- [【springboot源码分析】springboot 启动流程（三）：准备IOC容器](https://my.oschina.net/funcy/blog/4884127)
- [【springboot源码分析】springboot 启动流程（四）：启动IOC容器](https://my.oschina.net/funcy/blog/4888129)
- [【springboot源码分析】springboot 启动流程（五）：完成启动](https://my.oschina.net/funcy/blog/4906553)
- [【springboot源码分析】springboot 启动流程（六）：启动流程总结](https://my.oschina.net/funcy/blog/4906588)

------------

- [【springboot源码分析】springboot 自动装配（一）：加载自动装配类](https://my.oschina.net/funcy/blog/4870868)
- [【springboot源码分析】springboot 自动装配（二）：条件注解（一）](https://my.oschina.net/funcy/blog/4918863)
- [【springboot源码分析】springboot 自动装配（三）：条件注解（二）](https://my.oschina.net/funcy/blog/4921590)
- [【springboot源码分析】springboot 自动装配（四）：自动装配顺序](https://my.oschina.net/funcy/blog/4921594)

------------

- [【springboot源码分析】springboot web应用（一）：servlet 组件的注册流程](https://my.oschina.net/funcy/blog/4951050)
- [【springboot源码分析】springboot web应用（二）：WebMvc 装配过程](https://my.oschina.net/funcy/blog/4921595)

------------

- [【springboot打包与启动】01. maven引入及打包方式](https://my.oschina.net/funcy/blog/3145152)
- [【springboot打包与启动】02. 启动方式](https://my.oschina.net/funcy/blog/3154618)
- [【springboot打包与启动】03. "java -jar"启动分析](https://my.oschina.net/funcy/blog/3142868)


------------

如果您喜欢本文，欢迎关注公众号**Java技术探秘**，让我们一起在技术的世界里探秘吧！

![](https://oscimg.oschina.net/oscnet/up-45c189248711a568c9123c621ee7fe83f2c.png)

