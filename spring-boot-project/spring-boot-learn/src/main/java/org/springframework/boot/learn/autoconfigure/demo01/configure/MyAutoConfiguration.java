package org.springframework.boot.learn.autoconfigure.demo01.configure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ｛这里添加描述｝
 *
 * @author chengyan
 * @date 2020-12-31 10:30 下午
 */
@Configuration
public class MyAutoConfiguration {

	@Bean
	public Object object() {
		System.out.println("create object");
		return new Object();
	}

}
