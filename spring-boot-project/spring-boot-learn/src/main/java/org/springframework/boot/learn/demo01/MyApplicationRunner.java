package org.springframework.boot.learn.demo01;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * ｛这里添加描述｝
 *
 * @author chengyan
 * @date 2021-01-16 4:41 下午
 */
@Component
public class MyApplicationRunner implements ApplicationRunner {

	@Override
	public void run(ApplicationArguments args) throws Exception {
		System.out.println("MyApplicationRunner: hello world");
	}

}
