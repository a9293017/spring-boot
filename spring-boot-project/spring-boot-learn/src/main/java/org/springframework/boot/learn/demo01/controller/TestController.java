package org.springframework.boot.learn.demo01.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ｛这里添加描述｝
 *
 * @author chengyan
 * @date 2020-12-21 10:31 下午
 */
@RestController
@RequestMapping("/test")
public class TestController {

	@RequestMapping("hello")
	public String hello() {
		return "nice to meet you!";
	}

}
