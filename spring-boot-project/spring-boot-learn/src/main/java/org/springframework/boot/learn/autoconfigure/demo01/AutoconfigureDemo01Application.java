package org.springframework.boot.learn.autoconfigure.demo01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ｛这里添加描述｝
 *
 * @author chengyan
 * @date 2020-12-20 3:59 下午
 */
@SpringBootApplication
public class AutoconfigureDemo01Application {

	public static void main(String[] args) {
		SpringApplication.run(AutoconfigureDemo01Application.class, args);
	}

}
