package org.springframework.boot.learn.demo01;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * ｛这里添加描述｝
 *
 * @author chengyan
 * @date 2021-01-16 4:42 下午
 */
@Component
public class MyCommandLineRunner implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		System.out.println("MyCommandLineRunner: hello world!");
	}

}
